$(function() {
    var current = 1;

    var iterate = function() {
        var i = parseInt(current + 1);
        var lis = $('#rotmenu').children('li').size();
        if (i > lis) i = 1;
        display($('#rotmenu li:nth-child(' + i + ')'));
    }
    display($('#rotmenu li:first'));
    var slidetime = setInterval(iterate, 5000);

    $('#rotmenu li').bind('click', function(e) {
        clearTimeout(slidetime);
        display($(this));
        e.preventDefault();
    });

    function isEmpty( el ){
        return !$.trim(el.html());
    }

    function display(elem) {
        var $this = elem;
        var repeat = false;
        if (current == parseInt($this.index() + 1))
            repeat = true;

        if (!repeat)
            $this.parent().find('li:nth-child(' + current + ') a.listitem').stop(true, true).animate({
                'paddingLeft': '20px'
            }, 300, function() {
                $(this).animate({
                    'opacity': '0.75'
                }, 700);
            }).removeClass('selected');

        current = parseInt($this.index() + 1);

        var elem = $('a.listitem', $this);

        elem.stop(true, true).animate({
            'paddingLeft': '30px',
            'opacity': '1.0'
        }, 300).addClass('selected');

        var info_elem = elem.next();
        $('#rot1 .heading').stop().fadeOut('fast', function() {
            $('h1', $(this)).html(info_elem.find('.info_heading').html());
            $(this).fadeIn('slow');
        });

        $('#rot1 .description').stop().fadeOut('fast', function() {
            $('div', $(this)).html(info_elem.find('.info_description_left').html());
            $('p', $(this)).html(info_elem.find('.info_description').html());
            if (isEmpty($('div.left'))) {
                $('div.left').css("display", "none");
            } else {
                $('div.left').css("display", "block");
            }
            $(this).fadeIn('slow');
        })
        $('#rot1').prepend(
            $('<img/>', {
                style: 'opacity:0',
                class: 'bg'
            }).load(
                function() {
                    $(this).animate({
                        'opacity': '1'
                    }, 600);
                    $('#rot1 img:first').next().animate({
                        'opacity': '0'
                    }, 700, function() {
                        $(this).remove();
                    });
                }
            ).attr('src', '{{ paths.files }}playlist-slider/' + info_elem.find('.info_image').html())
        );
    }
});
